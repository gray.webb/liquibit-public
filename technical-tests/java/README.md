# Technical Tests

## Java Technical Test - Binance Futures Orderbook Parser

## Overview
This technical test requires you to write a simple Java application that parses the orderbook for Binance futures and prints updates to the top 5 levels. A stretch goal is to expose an internal API that conforms to the OpenAPI specification.

## Objectives
- **Primary Objective**: Successfully parse and print updates to the top 5 levels of the orderbook from the Binance futures market.
- **Stretch Goal**: Develop an internal REST API for the application that might typically be used by a Quant Analyst on a trading desk for developing a trading strategy, and ensure it adheres to the OpenAPI specification.

## Requirements
- The application should be written in Java.
- Use any libraries or frameworks you find appropriate.
- Ensure the application can handle high-frequency updates typical to exchange order books.

## Submission Guidelines
- Clone this repository to your local machine.
- Develop your solution locally, commit your changes, and push them back to this repository.
- Make sure your final submission includes all source files, build scripts, and documentation necessary to run the application.

## What We Are Looking For
- **Code Quality**: Clean, modular, and robust Java code.
- **Performance**: Efficient algorithms to handle real-time updates.
- **Documentation**: Clear, concise documentation of your design approach and choices.
- **Testing**: Unit and integration tests covering key components.
- **Version Control**: Meaningful commit messages that reflect your development process.

## Stretch Goals
- **API Implementation**: Implement an internal API for the application that abstracts away any exchange-specific implementation
- **API Documentation**: Provide a clear, concise API documentation, adhering to the OpenAPI specification, suitable for use by a Quant developer on a trading desk.

## Resources
- [Binance Futures API Documentation](https://binance-docs.github.io/apidocs/futures/en/#general-info)
- [OpenAPI Specification](https://swagger.io/specification/)

## Deadline
- Please submit your final solution by `2024-06-28 08:00` UTC.
